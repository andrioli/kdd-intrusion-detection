knn <- function(train, example, k = 10) {
  dist = sort(sqrt(rowSums(t((t(train) - example) ^ 2))))
  return (mean(dist[1:k]))
}

classifyAll <- function(train, test, k = 10) {
  dist = matrix(ncol=1, nrow=nrow(test))
  for (i in 1:nrow(dist)) {
    v = knn(train, test[i,], k)
    print(i)
    dist[i,] = v
  }
  return (dist)
}
