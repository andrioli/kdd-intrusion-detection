import java.util.Random;

public class ArrayUtils {

	private static void shuffle(int[] values, Random rnd) {
		int swap;
		for (int i = values.length; i > 1; i--) {
			int j = rnd.nextInt(i);
			swap = values[i - 1];
			values[i - 1] = values[j];
			values[j] = swap;
		}
	}
	
	public static void shuffle(int[] array) {
		ArrayUtils.shuffle(array, new Random());
	}

}
