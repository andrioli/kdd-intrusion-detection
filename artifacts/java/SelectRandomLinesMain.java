import java.io.IOException;


public final class SelectRandomLinesMain {
	
	public static void main(String[] args) throws IOException {
		try {
			
			int intArgs[] = getArgument(args);
			
			SelectRandomLines randomLines = new SelectRandomLines(
					System.in, intArgs[0], intArgs[1]);
			
			String line = randomLines.next();
			while (line != null) {
				System.out.println(line);
				line = randomLines.next();
			}
			
		} catch (IllegalArgumentException e) {
			System.err.println(e.getMessage());
		}
	}
	
	private static int[] getArgument(String[] args) {
		if (args.length != 2) {
			throw new IllegalArgumentException(
					"Should have two argument. Number of samples and number of lines");
		}
		
		// Get the number of samples and the number of lines
		int[] intArgs = new int[2];
		
		try {
			intArgs[0] = Integer.valueOf(args[0]);
		} catch (NumberFormatException e) {
			// Oops! Invalid argument
			throw new IllegalArgumentException(
					"Invalid argument, number of samples:" + args[0]);
		}
		try {
			intArgs[1] = Integer.valueOf(args[1]);
		} catch (NumberFormatException e) {
			// Oops! Invalid argument
			throw new IllegalArgumentException(
					"Invalid argument, number of lines:" + args[1]);
		}
		
		return intArgs;
	}
	

}
