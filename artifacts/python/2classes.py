#!/usr/bin/env python
import optparse
import sys
import re
import fileinput as fi
import string

"""Read the default input stream. If the attibute class is different of normal then change it to anomaly"""
"""Print the dataset with only two classes: normal and anomaly"""

def main():
        data = []
        for line in fi.input():
                data = line.split(",")
                if data[-1] != "normal.\n":
                        data[-1] = "anomaly"
		else:
			data[-1] = "normal"
                print string.join(data, ',')

if __name__ == '__main__':
        try:
                main()
        except Exception, e:
                pass
