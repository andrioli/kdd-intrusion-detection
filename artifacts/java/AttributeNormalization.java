import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class AttributeNormalization {

	public static void main(String[] args) throws IOException {
		
		Object[] o = getArgument(args);
		File file1 = (File) o[0];
		File file2 = (File) o[1];
		int values = (Integer) o[2];
		
		BufferedReader reader = new BufferedReader(new FileReader(file1));
		
		int lines = 0;
		
		double[] s = new double[values]; // Sum
		double[] q = new double[values]; // Sum of powers
		
		String line;
		while ((line = reader.readLine()) != null) {
			String[] strValues = line.split(",");
			for (int i = 0; i < values; i++) {
				double value = Double.valueOf( strValues[i] );
				s[i] = s[i] + value;
				q[i] = q[i] + value * value;
			}
			lines++;
		}
		
		for (int i = 0; i < values; i++) {
			s[i] = s[i] / lines; // Now hold the means
			q[i] = Math.sqrt((q[i] / lines) - s[i] * s[i]); // Now hold the Standard deviation
		}
		
		reader.close();
		reader = new BufferedReader(new FileReader(file2));
		
		while ((line = reader.readLine()) != null) {
			String[] strValues = line.split(",");
			for (int i = 0; i < values; i++) {
				
				double value = Double.valueOf( strValues[i] );
				
				value = (value - s[i]) / q[i]; // Normalize (v - mean) / sd
				
				System.out.format("%.4f,", value);
				
			}
			System.out.println( strValues[values] );
		}
		
		reader.close();
		
	}
	
	private static Object[] getArgument(String[] args) {
		if (args.length < 3) {
			throw new IllegalArgumentException(
					"Should have at least three argument");
		}
		
		File file1 = new File(args[0]);
		
		if (!file1.isFile()) {
			throw new IllegalArgumentException(
					"Argument file should be a valid file");
		}
		
		File file2 = new File(args[1]);
		
		if (!file1.isFile()) {
			throw new IllegalArgumentException(
					"Argument file should be a valid file");
		}
		
		Integer values = Integer.parseInt(args[2]);
		
		return new Object[] {file1, file2, values};
	}

}
