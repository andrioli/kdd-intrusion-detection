#!/usr/bin/env python
import optparse
import sys
import re
import fileinput as fi
import string

"""Read the default input stream. If the attibute class is different of normal then change it to an class of anomaly"""
"""Print the dataset with five classes: normal and anomaly (1 <- probe, 2 <- dos, 3 <- u2r, 4 <- r2l)"""

category = {}

category["normal."] = 0

# test attack types (only)

category["apache2."] = 2
category["httptunnel."] = 3
category["mailbomb."] = 2
category["mscan."] = 1
category["named."] = 4
category["processtable."] = 2
category["ps."] = 3
category["saint."] = 1
category["sendmail."] = 4
category["snmpgetattack."] = 4
category["snmpguess."] = 4
category["sqlattack."] = 3
category["udpstorm."] = 2
category["worm."] = 4
category["xlock."] = 4
category["xsnoop."] = 4
category["xterm."] = 3

# training attack types

category["back."] = 2
category["buffer_overflow."] = 3
category["ftp_write."] = 4
category["guess_passwd."] = 4
category["imap."] = 4
category["ipsweep."] = 1
category["land."] = 2
category["loadmodule."] = 3
category["multihop."] = 4
category["neptune."] = 2
category["nmap."] = 1
category["perl."] = 3
category["phf."] = 4
category["pod."] = 2
category["portsweep."] = 1
category["rootkit."] = 3
category["satan."] = 1
category["smurf."] = 2
category["spy."] = 4
category["teardrop."] = 2
category["warezclient."] = 4
category["warezmaster."] = 4

def main():
        data = []
        for line in fi.input():
                data = line.split(",")
		data[-1] = str(category.get(data[-1][:-1])) # data[-1][:-1] Take the last attribute (class) and ignores special char '\n'
                print string.join(data, ',')

if __name__ == '__main__':
        try:
                main()
        except Exception, e:
                pass
