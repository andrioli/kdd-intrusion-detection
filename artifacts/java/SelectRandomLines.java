import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;

public class SelectRandomLines {
	
	private final BufferedReader reader;
	
	private final int[] values;
	
	private int pos = 0;
	
	private int currentLine = -1;
	
	public SelectRandomLines(InputStream in, int samples, int lines) {
		reader = new BufferedReader(new InputStreamReader(in));
		values = init(samples, lines);
	}
	
	private int[] init(int samples, int lines) {
		int[] values = new int[lines];
		for (int i = 0; i < lines; i++)
			values[i] = i;
		
		ArrayUtils.shuffle(values);
		Arrays.sort(values, 0, samples);
		return Arrays.copyOf(values, samples);
	}

	public String next() throws IOException {
		String line = null;
		if (pos < values.length) {
			int lineNumber = values[pos++];
			
			while (currentLine != lineNumber) {
				line = reader.readLine();
				currentLine++;
				
				if (line == null) 
					break;
			}
			
		}
		return line;
	}

}
