all:    build
build:
	mkdir -p target/artifacts/java
	javac artifacts/java/* -d target/artifacts/java

	mkdir -p target/artifacts/python
	cp artifacts/python/* target/artifacts/python
	
	mkdir -p target/artifacts/R
	cp artifacts/R/* target/artifacts/R
clean:
	rm -rf target/*
